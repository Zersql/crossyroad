﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowScript : MonoBehaviour
{
    [SerializeField] GameObject Player;
    [SerializeField] Vector3 StartPosition;
    internal static bool stillMove;

    private void Start()
    {
        stillMove = true;
    }

    private void Update()
    {
        if (stillMove)
        {
            transform.position = Vector3.Lerp(transform.position, Player.transform.position + StartPosition, 0.1f);
        }
    }
}
