﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using Random = UnityEngine.Random;

/// <summary>
/// Класс, отвечающий за генерацию ландшафта, транспорта и монет.
/// </summary>
public class Generator : MonoBehaviour
{
    private const int LAND = 0;
    private const int WATER = 1;
    private const int HIGHWAY = 2;
    private const int TreesPerItem = 15;
    private const int MaxSurfacesCount = 40;
    private const int CoinGenerationChance = 50;
    private const int MinMaxCoinZAxisCoord = 6;
    private const int MinMaxBaulkCarZAxisCoord = 19;
    private const float MinCarLifeTime = 8;
    private const float MaxCarLifeTime = 9;
    private const float MinBaulkLifeTime = 15;
    private const float MaxBaulkLifeTime = 18;


    [SerializeField] private GameObject player;
    [SerializeField] private GameObject coin;
    [SerializeField] private List<GameObject> car;
    [SerializeField] private List<GameObject> baulk;
    [SerializeField] private List<GameObject> surfaceToGenerate;
    [SerializeField] private List<GameObject> trees;
    [SerializeField] private List<GameObject> baulks;

    private List<GameObject> activeTrees = new List<GameObject>();
    private List<GameObject> inactiveTrees = new List<GameObject>();
    private List<GameObject> activeCars = new List<GameObject>();
    private List<GameObject> inactiveCars = new List<GameObject>();
    private List<GameObject> activeBaulks = new List<GameObject>();
    private List<GameObject> inactiveBaulks = new List<GameObject>();
    private List<GameObject> generatedSurfaces = new List<GameObject>();
    private Dictionary<int, List<GameObject>> inactiveSurfaces = new Dictionary<int, List<GameObject>>();
    private Vector3 spawnPosition = new Vector3(-7, 0, 0);
    private Vector3 rotated = new Vector3(0, 180, 0);
    private int surfacesAmount;

    private void Start()
    {
        inactiveSurfaces.Add(LAND, new List<GameObject>());
        inactiveSurfaces.Add(WATER, new List<GameObject>());
        inactiveSurfaces.Add(HIGHWAY, new List<GameObject>());

        for (int greenSurfaces = 0; greenSurfaces < 8; greenSurfaces++)
        {
            GenerateSurface(true, LAND);
        }

        for (int basicSurfaces = 0; basicSurfaces < 27; basicSurfaces++)
        {
            GenerateSurface(true);
        }
    }

    /// <summary>
    /// Проверка машин и балок на окончание движения.
    /// </summary>
    private void Update()
    {
        if (activeCars.Count > 0 && (activeCars[0].transform.position.z > MinMaxBaulkCarZAxisCoord || activeCars[0].transform.position.z < -MinMaxBaulkCarZAxisCoord))
        {
            activeCars[0].SetActive(false);
            inactiveCars.Add(activeCars[0]);
            activeCars.RemoveAt(0);
        }

        if (activeBaulks.Count > 0 && (activeBaulks[0].transform.position.z > MinMaxBaulkCarZAxisCoord || activeBaulks[0].transform.position.z < -MinMaxBaulkCarZAxisCoord))
        {
            activeBaulks[0].SetActive(false);
            inactiveBaulks.Add(activeBaulks[0]);
            activeBaulks.RemoveAt(0);
        }
    }

    /// <summary>
    /// Генерация плит.
    /// </summary>
    /// <param name="isStart">Является ли время вызова стартом игры.</param>
    /// <param name="surfaceNumber">Номер генерируемой плиты.</param>
    internal void GenerateSurface(bool isStart, int surfaceNumber = -1)
    {
        int direction = 1;
        surfaceNumber = surfaceNumber == -1 ? Random.Range(0, surfaceToGenerate.Count) : surfaceNumber;
        surfacesAmount = GetAmount(isStart);

        if (generatedSurfaces.Count < MaxSurfacesCount)
        {
            for (int i = 0; i < surfacesAmount; i++)
            {
                int isCoinGenerated = Random.Range(0, 100);
                Vector3 coinPosition = spawnPosition + new Vector3(0, 1, Random.Range(-3, 3));

                if (surfaceNumber == LAND)
                {
                    GenerateTrees(isStart);

                    if (isCoinGenerated < CoinGenerationChance)
                    {
                        GenerateCoin(coinPosition);
                    }
                }
                else if(surfaceNumber == WATER)
                {
                    GenerateBaulks(spawnPosition, direction);
                }
                else
                {
                    GenerateCars(spawnPosition);

                    if (isCoinGenerated < CoinGenerationChance)
                    {
                        GenerateCoin(coinPosition);
                    }
                }
                TakeFromPoolOrCreateNew(surfaceNumber);
            }
        }
        else if (player.transform.position.x - generatedSurfaces[0].transform.position.x > 6 && generatedSurfaces[generatedSurfaces.Count - 1]?.transform.position.x - player.transform.position.x < 30)
        {
            for (int i = 0; i < surfacesAmount; i++)
            {
                int isCoinGenerated = Random.Range(0, 100);
                Vector3 coinPosition = spawnPosition + new Vector3(0, 1, Random.Range(-MinMaxCoinZAxisCoord, MinMaxCoinZAxisCoord));

                if (surfaceNumber == LAND)
                {
                    GenerateTrees(false);

                    if (isCoinGenerated < CoinGenerationChance)
                    {
                        GenerateCoin(coinPosition);
                    }
                }
                else if (surfaceNumber == WATER)
                {

                    GenerateBaulks(spawnPosition, direction);
                }
                else
                {
                    GenerateCars(spawnPosition);

                    if (isCoinGenerated < CoinGenerationChance)
                    {
                        GenerateCoin(coinPosition);
                    }
                }
                TakeFromPoolOrCreateNew(surfaceNumber);
                AddToPool();
            }
        }
        else
        {
            AddToPool();
        }
    }

    /// <summary>
    /// Проверка на наличие дерева для генерации монет.
    /// </summary>
    /// <param name="position">Место генерации.</param>
    /// <returns>Можно ли сгенерировать монету.</returns>
    private bool CheckForTrees(Vector3 position)
    {
        var colliders = Physics.OverlapSphere(position + new Vector3(0, 0.5f, 0), 0.8f);
        if (colliders.Length < 1)
            return false;
        return true;
    }

    /// <summary>
    /// Генерация монет.
    /// </summary>
    /// <param name="position">Место генерации.</param>
    private void GenerateCoin(Vector3 position)
    {
        Vector3 coinPosition = spawnPosition + new Vector3(0, 1, Random.Range(-MinMaxCoinZAxisCoord, MinMaxCoinZAxisCoord));
        if (!CheckForTrees(coinPosition))
        {
            Instantiate(coin, position, Quaternion.Euler(new Vector3(0, Random.Range(0, 180))));
        }
    }

    /// <summary>
    /// Генерация машин.
    /// </summary>
    /// <param name="surfacePosition">Позиция плиты, по которой будет двигаться машина.</param>
    private void GenerateCars(Vector3 surfacePosition)
    {
        int t = Random.Range(0, 2);
        int model = Random.Range(0, car.Count);

        if (t == 1)
        {
            Vector3 position = surfacePosition + new Vector3(0, 1, -19);
            StartCoroutine(SpawnVehicle(position, model));
        }
        else
        {
            Vector3 position = surfacePosition + new Vector3(0, 1, 19);
            StartCoroutine(SpawnVehicle(position, model));
        }
    }

    /// <summary>
    /// Генерация балок.
    /// </summary>
    /// <param name="surfacePosition">Позиция плиты, по которой будет двигаться балка.</param>
    private void GenerateBaulks(Vector3 surfacePosition, int direction)
    {
        int t = Random.Range(0, 2);
        int model = Random.Range(0, baulk.Count);

        if (t == 1)
        {
            Vector3 position = surfacePosition + new Vector3(0, 0.25f, -19);
            StartCoroutine(SpawnBaulks(position, model));

        }
        else
        {
            Vector3 positio1n = surfacePosition + new Vector3(0, 0.25f, 19);
            StartCoroutine(SpawnBaulks(positio1n, model));
        }
    }

    /// <summary>
    /// Переодическая генерация машин.
    /// </summary>
    /// <param name="position">Место спавна.</param>
    /// <param name="model">Вид машины.</param>
    /// <returns>Время между спавном.</returns>
    private IEnumerator SpawnVehicle(Vector3 position, int model)
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(3f, 5f));
            SpawnMovableObjects(position, car[model], activeCars, inactiveCars, MinCarLifeTime, MaxCarLifeTime);

        }
    }

    /// <summary>
    /// Переодическая генерация балок.
    /// </summary>
    /// <param name="position">Место спавна.</param>
    /// <param name="model">Вид машины.</param>
    /// <returns>Время между спавном.</returns>
    private IEnumerator SpawnBaulks(Vector3 position, int model)
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(3f, 5f));
            SpawnMovableObjects(position, baulk[model], activeBaulks, inactiveBaulks, MinBaulkLifeTime, MaxBaulkLifeTime);
        }
    }

    /// <summary>
    /// Метод, производящий спавн двигающихся объектов.
    /// </summary>
    /// <param name="position">Место спавна.</param>
    /// <param name="objForSpawn">Объект спавна.</param>
    /// <param name="activeItems">Список активных объектов типа спавнящегося.</param>
    /// <param name="inactiveItems">Список деактивированных объектов.</param>
    /// <param name="minLifeTimeMaxSpeed">Максимальная скорость.</param>
    /// <param name="maxLifeTimeMinSpeed">Минимальная скорость.</param>
    private void SpawnMovableObjects(Vector3 position, GameObject objForSpawn, List<GameObject> activeItems, List<GameObject> inactiveItems, float minLifeTimeMaxSpeed, float maxLifeTimeMinSpeed)
    {
        if (player && player.transform.position.x - position.x < 6)
        {
            if (inactiveItems.Count < 1)
            {
                if (position.z > 0)
                {   
                    activeItems.Add(Instantiate(objForSpawn, position, Quaternion.AngleAxis(180, Vector3.up)));
                }
                else 
                {
                    activeItems.Add(Instantiate(objForSpawn, position, Quaternion.identity));
                }
            }
            else
            {
                var current = inactiveItems[0];
                activeItems.Add(current);
                inactiveItems.RemoveAt(0);
                current.SetActive(true);

                int t = Random.Range(0, 2);
                current.transform.position = position;
                if (current.transform.position.z > 0)
                {
                    if (!current.transform.rotation.eulerAngles.Equals(rotated))
                    {
                        current.transform.Rotate(rotated);
                    }
                    current.transform.DOMoveZ(-20, Random.Range(minLifeTimeMaxSpeed, maxLifeTimeMinSpeed));
                }
                else
                {
                    if (current.transform.rotation.eulerAngles.Equals(rotated))
                    {
                        current.transform.Rotate(rotated);
                    }
                    current.transform.DOMoveZ(20, Random.Range(minLifeTimeMaxSpeed, maxLifeTimeMinSpeed));
                }


            }

        }
    }

    /// <summary>
    /// Проверка на пустоту пула балок.
    /// </summary>
    /// <param name="surfaceNumber">Номер необходимой поверхности.</param>
    private void TakeFromPoolOrCreateNew(int surfaceNumber)
    {
        if (inactiveSurfaces[surfaceNumber].Count > 0)
        {
            inactiveSurfaces[surfaceNumber][0].transform.position = spawnPosition;
            generatedSurfaces.Add(inactiveSurfaces[surfaceNumber][0]);
            inactiveSurfaces[surfaceNumber][0].SetActive(true);
            inactiveSurfaces[surfaceNumber].RemoveAt(0);
            spawnPosition.x++;
        }
        else
        {
            generatedSurfaces.Add(Instantiate(surfaceToGenerate[surfaceNumber], spawnPosition, Quaternion.identity));
            spawnPosition.x++;
        }
    }

    /// <summary>
    /// Генерация деревьев.
    /// </summary>
    /// <param name="isStart">Является ли время вызова началом игры.</param>
    private void GenerateTrees(bool isStart)
    {
        for (int j = 0; j < TreesPerItem; j++)
        {
            int t = Random.Range(-25, 25);

            if (isStart && t == 0)
            {
                t++;
            }
            else if(!isStart && t == Random.Range(-3,3)) 
            {
                t++;
            }

            if (inactiveTrees.Count < 1)
            {
                activeTrees.Add(Instantiate(trees[Random.Range(0, trees.Count)], spawnPosition + new Vector3(0, 0.5f, t), Quaternion.identity));
            }
            else
            {
                inactiveTrees[0].transform.position = spawnPosition + new Vector3(0, 0.5f, t);
                inactiveTrees[0].SetActive(true);
                activeTrees.Add(inactiveTrees[0]);
                inactiveTrees.RemoveAt(0);
            }
        }
    }

    /// <summary>
    /// Добавление в пул.
    /// </summary>
    private void AddToPool()
    {

        if (generatedSurfaces[0].GetComponent<MeshRenderer>().material.name.StartsWith("Land"))
        {
            inactiveSurfaces[LAND].Add(generatedSurfaces[0]);

            for (int i = 0; i < TreesPerItem; i++)
            {
                activeTrees[0].SetActive(false);
                inactiveTrees.Add(activeTrees[0]);
                activeTrees.RemoveAt(0);
            }
        }
        else if (generatedSurfaces[0].GetComponent<MeshRenderer>().material.name.StartsWith("Water"))
        {
            inactiveSurfaces[WATER].Add(generatedSurfaces[0]);
        }
        else if(generatedSurfaces[0].GetComponent<MeshRenderer>().material.name.StartsWith("Highway"))
        {
            inactiveSurfaces[HIGHWAY].Add(generatedSurfaces[0]);
        }

        generatedSurfaces[0].SetActive(false);
        generatedSurfaces.RemoveAt(0);
    }

    /// <summary>
    /// Количество плиток подряд.
    /// </summary>
    /// <param name="isStart">Является ли время вызова началом игры.</param>
    /// <returns>Количество плит.</returns>
    private int GetAmount(bool isStart)
    {
        if (isStart)
        {
            return 1;
        }
        else
        {
            int t = UnityEngine.Random.Range(0, 100);

            if (t < 40)
            {
                return 1;
            }
            else if (t < 85 && t >= 40)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }
    }
}
