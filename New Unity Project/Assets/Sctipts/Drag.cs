﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Контроль свайпов.
/// </summary>
public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    internal static bool moveLeft = false;
    internal static bool moveRight = false;
    internal static bool moveForward = false;
    internal static bool moveBack = false;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if ((Mathf.Abs(eventData.delta.x)) > (Mathf.Abs(eventData.delta.y)))
        {
            if (eventData.delta.x > 0)
            {
                moveRight = true;
            }
            else
            {
                moveLeft = true;
            }
        }
        else
        {
            if (eventData.delta.y > 0)
            {
                moveForward = true;
            }
            else
            {
                moveBack = true;
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
    }
}
