﻿using System.Collections;
using UnityEngine;
using System;
using DG.Tweening;
using UnityEngine.UI;

/// <summary>
/// Класс, производящий контроль смерти игрока от прыжков в воду, машин; конролирует прыжки на балку и звуки.
/// </summary>
public class StopGameScript : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject anim;
    [SerializeField] private Canvas resultScreen;
    [SerializeField] private Camera mainCam;
    [SerializeField] private Text recordScore;
    [SerializeField] private GameObject deadModel;

    private int soundsIsOn;
    private float delta;
    private static int onColliderCoins;
    internal static AudioSource jump;
    internal static AudioSource coinGain;


    private void Start()
    {
        soundsIsOn = PlayerPrefs.GetInt("Sounds");
        onColliderCoins = PlayerPrefs.GetInt("Coins", 0);
        if (soundsIsOn == 1)
        {
            SetSounds();
        }
    }

    /// <summary>
    /// Установка звуков.
    /// </summary>
    private void SetSounds()
    {
        var t = player.GetComponents<AudioSource>();
        coinGain = t[0];
        jump = t[1];
    }

    /// <summary>
    /// Контроль смерти от воды или машины.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "car" || other.transform.tag == "water")
        {
            if (other.transform.tag == "car")
            {
                CreateDead();
            }
            if (MainPlayerScript.maxScoreCurrentGame > PlayerPrefs.GetInt("Record", 0))
            {
                PlayerPrefs.SetInt("Record", MainPlayerScript.maxScoreCurrentGame);
                PlayerPrefs.Save();
            }
            FollowScript.stillMove = false;
            mainCam.DOOrthoSize(mainCam.transform.position.y - 2, 1);
            mainCam.transform.DOMoveY(mainCam.transform.position.y - 2, 1);
            recordScore.text = $"RECORD {PlayerPrefs.GetInt("Record")}";
            resultScreen.enabled = true;
            Destroy(player);
        }
    }
    
    private void CreateDead()
    {

        Instantiate(deadModel, player.transform.position + new Vector3(0, -1, 0), Quaternion.identity);
    }

    /// <summary>
    /// Проверка на взятие монет или прыжка на балку.
    /// </summary>
    /// <param name="collision">Коллизия соприкосающегося объекта.</param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponentInChildren<Baulk>())
        {
            StartCoroutine(ChangeParent(0.05f, collision));
        }
        else if (collision.collider.GetComponent<Coin>())
        {
            if(coinGain)
            {
                coinGain.Play();
            }
            Destroy(collision.collider.gameObject);
            PlayerPrefs.SetInt("Coins", ++onColliderCoins);
            PlayerPrefs.Save();
        }
        else
        {
            if (player.transform.position.z % 1 != 0)
            {
                float delta = (float)Math.Round(player.transform.position.z) - player.transform.position.z;
                player.transform.position += new Vector3(0, 0, delta);
            }
        }
    }

    private IEnumerator ChangeParent(float time, Collision collision)
    {
        yield return new WaitForSeconds(time);
        transform.parent = collision.collider.transform;
    }

    /// <summary>
    /// Возвращает родительский объект при касании земли.
    /// </summary>
    /// <param name="collision">Коллизия.</param>
    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.GetComponent<Baulk>())
        {
            transform.parent = anim.transform;
        }
    }
}
