﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sounds : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private Sprite buttonEnabled;
    [SerializeField] private Sprite buttonDisabled;

    public static bool isOn;

    public void Start()
    {
        if (PlayerPrefs.GetInt("Sounds") == 0)
        {
            button.image.sprite = buttonDisabled;
        }
        else
        {
            button.image.sprite = buttonEnabled;
        }
    }

    public void EnableOrDisable()
    {
        if(isOn)
        {
            isOn = false;
            PlayerPrefs.SetInt("Sounds", 0);
            button.image.sprite = buttonDisabled;
        }
        else
        {
            isOn = true;
            PlayerPrefs.SetInt("Sounds", 1);
            button.image.sprite = buttonEnabled;
        }
    }

}
