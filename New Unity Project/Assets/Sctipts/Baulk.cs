﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Baulk : MonoBehaviour
{
    void Start()
    {
        float lifeTime = Random.Range(15, 18);
        if (transform.position.z > 0)
        {
            transform.DOMoveZ(-20, lifeTime);
        }
        else
        {
            transform.DOMoveZ(20, lifeTime);
        }
    }
}
