﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

/// <summary>
/// Класс, контролирующий движение, состояние игры и контролирует афк.
/// </summary>
public class MainPlayerScript : MonoBehaviour
{
    private const int MinMaxZPosition = 6;

    private static readonly Vector3 MOVEFORWARD = new Vector3(1, 0, 0);
    private static readonly Vector3 MOVERIGHT = new Vector3(0, 0, -1);
    private static readonly Vector3 MOVELEFT = new Vector3(0, 0, 1);
    private static readonly Vector3 MOVEBACK = new Vector3(-1, 0, 0);

    [SerializeField] private GameObject player;
    [SerializeField] private GameObject eagle;
    [SerializeField] private Generator Generator;
    [SerializeField] private Text score;
    [SerializeField] private Canvas startScreen;
    [SerializeField] private Button startButton;
    [SerializeField] private Text coinCountField;
    [SerializeField] private Text recordScore;
    [SerializeField] private Canvas resultScreen;
    [SerializeField] private Camera mainCam;

    internal static int maxScoreCurrentGame;

    private Animator animator;
    private int sAndwDifference = 0;
    private int currentPosition = 0;
    private bool canMove = true;
    private bool gameOngoing = false;
    private float afkDeathCheck = 0;


    private void Start()
    {
        maxScoreCurrentGame = 0;
        animator = GetComponent<Animator>();
        resultScreen.enabled = false;
    }

    /// <summary>
    /// Проверка на движение, контроль количества собранных монет.
    /// </summary>
    private void Update()
    {
        coinCountField.text = PlayerPrefs.GetInt("Coins", 0).ToString();
        if (afkDeathCheck > 8 && player)
        {
            EagleDeath();
            afkDeathCheck = 0;
        }

        if (player && (player.transform.position.z < -MinMaxZPosition || player.transform.position.z > MinMaxZPosition || player.transform.position.y > 4 || player.transform.position.y < 0))
        {
            if (maxScoreCurrentGame > PlayerPrefs.GetInt("Record", 0))
            {
                PlayerPrefs.SetInt("Record", maxScoreCurrentGame);
                PlayerPrefs.Save();
            }
            FollowScript.stillMove = false;
            mainCam.DOShakePosition(3, 0.7f, 2);
            Invoke("DestroyPlayer", 1f);
            EnableResultScreen();
        }

        if (!gameOngoing)
        {
            if (!startButton.enabled)
            {
                gameOngoing = true;
                startScreen.enabled = false;
            }
        }
        else
        {
            afkDeathCheck = afkDeathCheck + Time.deltaTime;
            startScreen.enabled = false;

            if (currentPosition > maxScoreCurrentGame)
            {
                maxScoreCurrentGame = currentPosition;
                score.text = $" {maxScoreCurrentGame}";
            }

            if ((Input.GetKeyDown(KeyCode.W) || Drag.moveForward) && canMove)
            {
                Drag.moveForward = false;
                MoveWithWoodsCheck(MOVEFORWARD, true);
                player.transform.DORotate(new Vector3(0, 0, 0), 0.2f, RotateMode.Fast);
            }
            else if ((Input.GetKeyDown(KeyCode.D) || Drag.moveRight) && canMove)
            {
                Drag.moveRight = false;
                MoveWithWoodsCheck(MOVERIGHT);
                player.transform.DORotate(new Vector3(0, 90, 0), 0.2f, RotateMode.Fast);

            }
            else if ((Input.GetKeyDown(KeyCode.A) || Drag.moveLeft) && canMove)
            {
                Drag.moveLeft = false;
                MoveWithWoodsCheck(MOVELEFT);
                player.transform.DORotate(new Vector3(0, 270, 0), 0.2f, RotateMode.Fast);
            }
            else if (Input.GetKeyDown(KeyCode.S) || Drag.moveBack && canMove)
            {
                Drag.moveBack = false;
                MoveWithWoodsCheck(MOVEBACK, false);
                player.transform.DORotate(new Vector3(0, 180, 0), 0.2f, RotateMode.Fast);
            }
            if (sAndwDifference < -2)
            {
                EagleDeath();
            }
        }
    }

    /// <summary>
    /// Приближение камеры при смерти.
    /// </summary>
    private void DeathCamMove()
    {
        if (maxScoreCurrentGame > PlayerPrefs.GetInt("Record", 0))
        {
            PlayerPrefs.SetInt("Record", maxScoreCurrentGame);
            PlayerPrefs.Save();
        }

        FollowScript.stillMove = false;
        mainCam.DOOrthoSize(mainCam.transform.position.y - 2, 1);
        mainCam.transform.DOMoveY(mainCam.transform.position.y - 2, 1);
    }

    /// <summary>
    /// Смерть от орла при афк либо движении назад.
    /// </summary>
    private void EagleDeath()
    {
        DeathCamMove();
        var t = Instantiate(eagle, player.transform.position + new Vector3(5, 1, -3), Quaternion.identity);
        t.transform.DOMove(t.transform.position - new Vector3(30, 0, 0), 2);
        sAndwDifference = 0;
        gameOngoing = false;
        EnableResultScreen();
        Invoke("DestroyPlayer", 0.3f);
    }

    /// <summary>
    /// Включает экран результата.
    /// </summary>
    private void EnableResultScreen()
    {
        recordScore.text = $"RECORD {PlayerPrefs.GetInt("Record")}";
        resultScreen.enabled = true;
    }

    private void DestroyPlayer()
    {
        Destroy(player);
    }

    /// <summary>
    /// Разрешить игроку движение.
    /// </summary>
    private void FinishJump()
    {
        canMove = true;
    }

    /// <summary>
    /// Проверка на наличие дерева в направлении движения.
    /// </summary>
    /// <param name="direction">Направление.</param>
    /// <param name="goesForward">Направлено ли движение вперед.</param>
    private void MoveWithWoodsCheck(Vector3 direction, bool? goesForward = null)
    {
        var colliders = Physics.OverlapSphere(player.transform.position + direction, 0.25f) ?? null;

        if (colliders.Length < 1 || !string.Equals(colliders[0].tag, "tree", System.StringComparison.InvariantCultureIgnoreCase))
        {
            afkDeathCheck = 0;
            if(StopGameScript.jump)
            {
                StopGameScript.jump.Play();
            }
            TransformPosition(direction);

            if (goesForward == true)
            {
                currentPosition++;
                if (sAndwDifference < 0)
                {
                    sAndwDifference++;
                }
                else
                {
                    Generator.GenerateSurface(false);
                }
            }
            else if (goesForward == false)
            {
                sAndwDifference--;
                currentPosition--;
            }
        }
    }

    /// <summary>
    /// Движение в случае отсутствия дерева.
    /// </summary>
    /// <param name="direction">Направление движения.</param>
    private void TransformPosition(Vector3 direction)
    {

        player.transform.DOMove(player.transform.position + direction, 0.14f);
        canMove = false;
        animator.SetTrigger("Jump");
    }
}