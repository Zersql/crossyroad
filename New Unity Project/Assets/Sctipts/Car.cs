﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Car : MonoBehaviour
{
    void Start()
    {
        float lifeTime = Random.Range(8, 9);
        if (transform.position.z > 0)
        {
            transform.DOMoveZ(-20, lifeTime);
        }
        else
        {
            transform.DOMoveZ(20, lifeTime);
        }
    }
}
